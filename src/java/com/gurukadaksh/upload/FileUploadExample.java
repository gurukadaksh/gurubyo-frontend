/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gurukadaksh.upload;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

public class FileUploadExample extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            // Create a factory for disk-based file items
            FileItemFactory factory = new DiskFileItemFactory();

            // Create a new file upload handler
            ServletFileUpload upload = new ServletFileUpload(factory);
            String uploadStatus = "";
            try {
                // Parse the request
                List /* FileItem */ items = upload.parseRequest(request);
                Iterator iterator = items.iterator();
                while (iterator.hasNext()) {
                    FileItem item = (FileItem) iterator.next();
                    if (!item.isFormField()) {
                        String fileName = item.getName();
                        String root = getServletContext().getRealPath("/");
                        File path = new File(root + "/uploads");
                        if (!path.exists()) {
                            boolean status = path.mkdirs();
                        }

                        File uploadedFile = new File(path + "/" + fileName);
                        System.out.println(uploadedFile.getAbsolutePath());                        
                        item.write(uploadedFile);
                        uploadStatus = "File Uploaded!";
                        request.setAttribute("UploadStatus", uploadStatus);
                        request.getRequestDispatcher("guruSearch.jsp").forward(request, response);
                    }
                }
            } catch (FileUploadException e) {
                e.printStackTrace();
                uploadStatus = "File Upload Failed!" + e;
                request.setAttribute("UploadStatus", uploadStatus);
                request.getRequestDispatcher("guruSearch.jsp").forward(request, response);
            } catch (Exception e) {
                uploadStatus = "File Upload Failed!" + e;
                request.setAttribute("UploadStatus", uploadStatus);
                request.getRequestDispatcher("guruSearch.jsp").forward(request, response);
                e.printStackTrace();
            }
        }
    }
}