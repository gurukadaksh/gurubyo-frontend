/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh.beans;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.Stateful;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Deepak Subramanian
 */
@Stateful
public class GurukadakshOperations
{
    private String gkToken = null;
    
    /**
     * Returns navbar string to HTML for many pages
     * @return 
     */
    public static String getNavBar()
    {
        return "<html lang='en'><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'> <meta charset='utf-8'>" +
        "<title>gurukadaksh</title>" +
        "<meta name='viewport' content='width=device-width, initial-scale=1.0'>" +
        "<meta name='description' content=''>" +
        "<meta name='author' content=''>" +

        "<link href='css/metro.css' rel='stylesheet'>" +
        "<link href='css/start.less?v=4' rel='stylesheet/less' type='text/css' >" +
        "<link href='css/representative.css' rel='stylesheet' media='all'>" +
        "<link href='css/bootstrap.css' rel='stylesheet'>" +
        "<style type='text/css'>" +
            "body {" +
                "padding-top: 60px;" +
                "padding-bottom: 40px;" +
            "}" +
        "</style>" +
        "<link href='css/bootstrap-responsive.css' rel='stylesheet'>" +
        "<!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->" +
        "<!--[if lt IE 9]>" +
          "<script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>" +
        "<![endif]-->" +
        "<link rel='shortcut icon' href='assets/gurukadakshThumbnailLogo2.png'>" +
        "<link rel='apple-touch-icon-precomposed' sizes='144x144' href='http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png'>" +
        "<link rel='apple-touch-icon-precomposed' sizes='114x114' href='http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png'>" +
        "<link rel='apple-touch-icon-precomposed' sizes='72x72' href='http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png'>" +
        "<link rel='apple-touch-icon-precomposed' href='http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png'>" +
    "</head>" +
    "<body>" +
        "<%= gurukadakshBean.getUserProfile()%>" +
        "<div class='navbar navbar-inverse navbar-fixed-top'>" +
            "<div class='navbar-inner'>" +
                "<div class='container'>" +
                    "<a href='/' class='brand'><img alt='Logobro_nav' id='topbar_logo' src='assets/gurubyoLogoFullInverted.png' width='200' height='20' /></a>" +
                    "<a class='btn btn-navbar' data-toggle='collapse' data-target='.nav-collapse'>" +
                        "<span class='icon-bar'></span>" +
                        "<span class='icon-bar'></span>" +
                        "<span class='icon-bar'></span>" +
                    "</a>		  	  " +
                    "<ul class='nav pull-right'>" +
                        "<li class='divider-vertical'></li>" +
                        "<li class='dropdown' id='menu1'>" +
                            "<a class='dropdown-toggle' data-toggle='dropdown' href='#menu1'>" +
                                "<img src='<%= gurukadakshBean.getUserProfilePicture()%>'  />" +
                                "Hi, <%= gurukadakshBean.getUserFirstName()%>" +
                                "<b class='caret'></b>" +
                            "</a>" +
                            "<ul class='dropdown-menu'>" +
                                "<li><a href='#'>Settings</a></li>" +
                                "<li><a href='#'>Help</a></li>" +
                                "<li class='divider'></li>" +
                                "<li><a href='logout.jsp'>Sign out</a></li>" +
                            "</ul>" +
                        "</li>" +
                    "</ul>" +
                "</div>" +
            "</div>" +
        "</div>" ;
    }

    public String getUserTags()
    {
        String out = "";
        try
        {
            String jsonString = makePostConnection( GurukadakshAPIMap.General.URLParams.gkToken + "=" + gkToken, GurukadakshAPIMap.UserTagApiUrl.userTagPage_URL);
            JSONArray givenArray = new JSONArray(jsonString);
            out = out + "<span class='label label-warning'>Skill(s) found </span><span class='badge badge-important'>"+ givenArray.length() +"</span><br><br>";
            
            for ( int i = 0 ; i < givenArray.length() ; i ++) 
            {
                
                out = out + "<span class='label "+ ((i%2==0)? "label-info":"label-inverse")+"'>" + givenArray.getString(i) + "</span> ";
            }
        }
        catch(Exception e)
        {
            out = e.toString();
        }
        finally
        {
            return out;
        }
    }
    public boolean isLoggedIn() ///< This function is used to check if the user is logged in or not.
    {
        boolean returnValue = false;
        try
        {
            returnValue = !(gkToken == null);
        }
        catch (Exception e)
        {
            returnValue = false;
        }
        finally
        {
            return returnValue;
        }
    }
    private class UserProfile ///< The class that represents the user profile.
    {
        public String userFirstName = null;
        public String userLastName = null;
        public String userProfilePicture = null;
        public String userHeadline = null;

        public String getUserHeadline()
        {
            return userHeadline;
        }

        public void setUserHeadline(String userHeadline)
        {
            this.userHeadline = userHeadline;
        }

        
        public String getUserFirstName()
        {
            return userFirstName;
        }

        public void setUserFirstName(String userFirstName)
        {
            this.userFirstName = userFirstName;
        }

        public String getUserLastName()
        {
            return userLastName;
        }

        public void setUserLastName(String userLastName)
        {
            this.userLastName = userLastName;
        }

        public String getUserProfilePicture()
        {
            return userProfilePicture;
        }

        public void setUserProfilePicture(String userProfilePicture)
        {
            this.userProfilePicture = userProfilePicture;
        }

    }

    UserProfile userProfile = null;
/**
 * The authentication url gets the linkedin url that is used to authenticate and then it is redirected back to the userpage.
 * @param request
 * @return
 */
    public String getAuthenticationUrl(HttpServletRequest request) 
    {
        String authenticationUrl = null;String jsonAuthentication = null;
        try
        {
           String redirectUrl = request.getScheme()+"://"+request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/User.jsp";// request.getServletPath();
            jsonAuthentication = makePostConnection( GurukadakshAPIMap.LoginApiUrl.POSTParams.callback + "=" + redirectUrl, GurukadakshAPIMap.LoginApiUrl.loginPage_URL);
            JSONObject authenticationObject = new JSONObject(jsonAuthentication);
            authenticationUrl = authenticationObject.getString(GurukadakshAPIMap.JSONParameters.AUTH_URL);
        }
        catch ( Exception exception )
        {
            authenticationUrl = null; 
            Logger.getLogger(GurukadakshOperations.class.getName()).log(Level.SEVERE, null, exception);
        }
        finally
        {
            return authenticationUrl;
        }
        
    }
    /**
     * This is used to get the various search results for the gurikadaksh skills Api
     * @param skills
     * @return
     */
    public String getSkillSearchResult(String skills)
    {
        String skillsResult = "";
        try
        {
            skillsResult = makeGetConnection(/*GurukadakshAPIMap.General.URLParams.gkToken+"="+gkToken+*/"i=2&"+GurukadakshAPIMap.SkillsSearchApiUrl.POSTParams.skillsToSearch+"="+URLEncoder.encode(skills, "UTF-8"),GurukadakshAPIMap.SkillsSearchApiUrl.skillsSearchPage_URL);
        }
        catch (Exception exception)
        {
            skillsResult="1.Exception is "+exception;
        }
        finally
        {
            return skillsResult;
        }
    }
/**
 * The user profile is obtained from the backend server. It currently contains the first name, last name, headline and the profile picture url
 */
    public void getUserProfile()
    {
        try
        {
            String JSONUserProfile = makePostConnection(GurukadakshAPIMap.General.URLParams.gkToken+"="+gkToken,GurukadakshAPIMap.UserProfileApiUrl.userProfilePage_URL);

            JSONObject userProfileObject = new JSONObject(JSONUserProfile);
            userProfile =  new UserProfile();
            userProfile.userFirstName = userProfileObject.getString(GurukadakshAPIMap.JSONParameters.USER_FIRSTNAME);
            userProfile.userLastName = userProfileObject.getString(GurukadakshAPIMap.JSONParameters.USER_LASTNAME);
            userProfile.userHeadline = userProfileObject.getString(GurukadakshAPIMap.JSONParameters.USER_HEADLINE);
            userProfile.userProfilePicture = userProfileObject.getString(GurukadakshAPIMap.JSONParameters.USER_PROFILEPIC);
        }
        catch ( Exception exception )
        {
            userProfile = null;
        }
        finally
        {

        }
    }
   
/**
 * This function gets the GKToken and keeps the token stored as part of the session bean.
 * @param request
 */
    public void setGKToken(HttpServletRequest request, HttpServletResponse response)
    {
        try
        {
            String gkToken_request = request.getParameter(GurukadakshAPIMap.General.URLParams.gkToken);
            if(gkToken_request != null && !gkToken_request.equals(this.gkToken))
            {
                this.gkToken = gkToken_request;
                response.sendRedirect(request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/User.jsp");
            }
        }
        catch ( Exception exception )
        {
            gkToken = null; this.checkAndRedirectLogin(request, response);
        }
        finally
        {
            if(gkToken != null && gkToken.trim().isEmpty())
            {
                gkToken = null; this.checkAndRedirectLogin(request, response);
            }

        }
    }
    public void checkAndRedirectLogin(HttpServletRequest request,HttpServletResponse response)
    {
        if(!this.isLoggedIn())
        {
            try {
                response.sendRedirect(request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/Login.jsp");
            } catch (IOException ex) {
                Logger.getLogger(GurukadakshOperations.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    public String getUserProfilePicture()
    {
        return userProfile.getUserProfilePicture();
    }

    public String getUserLastName() {
        return userProfile.getUserLastName();
    }

    public String getUserHeadline() {
        return userProfile.getUserHeadline();
    }

    public String getUserFirstName() {
        return userProfile.getUserFirstName();
    }

    /**
     * The create project function is used to create a given project from the backend.
     * @param request 
     */
 public long createProject(HttpServletRequest request)
    {
        long projectId =  0;
        try
        {
            String JSONCreateProject = makePostConnection(GurukadakshAPIMap.General.URLParams.gkToken+"="+gkToken+"&"+GurukadakshAPIMap.CreateProjectApiUrl.POSTParams.projectDescription+"="+request.getParameter(GurukadakshAPIMap.CreateProjectApiUrl.POSTParams.projectDescription)+"&"+GurukadakshAPIMap.CreateProjectApiUrl.POSTParams.projectName+"="+request.getParameter(GurukadakshAPIMap.CreateProjectApiUrl.POSTParams.projectName)+"&"+GurukadakshAPIMap.CreateProjectApiUrl.POSTParams.projectKeywords+"="+request.getParameter(GurukadakshAPIMap.CreateProjectApiUrl.POSTParams.projectKeywords),GurukadakshAPIMap.CreateProjectApiUrl.createProjectPage_URL);        
            JSONObject projectObject = new JSONObject(JSONCreateProject);
            projectId = projectObject.optLong("projectId");
        }
        catch ( Exception exception )
        {
            //JSONCreateProject = "Exception is "+ exception; 
            Logger.getLogger(GurukadakshOperations.class.getName()).log(Level.SEVERE, "Exception is {0}", exception);
        }
        finally
        {
            return projectId;
        }
    }
 /**
  * The searchGurus function is used to search for the various gurus given the project Identifier.
  * @param projectId
  * @return 
  */
 public String searchGurus(long projectId)
 {
     String JSONSearchGurus = "";
     String returnString = "";
     try
     {
          JSONSearchGurus = makePostConnection(GurukadakshAPIMap.General.URLParams.gkToken+"="+gkToken+"&"+GurukadakshAPIMap.GuruSearchApiUrl.POSTParams.projectId+"="+projectId, GurukadakshAPIMap.GuruSearchApiUrl.guruSearchPage_URL);
          JSONArray searchGurus = new JSONArray(JSONSearchGurus);
          if (searchGurus.length() > 1) {
              returnString += "Hurray! We found a few gurus <br>";
          }
          else if(searchGurus.length() ==1 ) {
              returnString += "We found a guru <br>";
          }
          else {
              returnString += "Perhaps, it's time you build your Gurubyo network!<br>";
          }
          returnString += "<ul class=thumbnails>";
          for (int i = 0 ; i < searchGurus.length() ; i++)
          {
              JSONObject guru = (JSONObject) searchGurus.get(i);
              returnString += "<li class=span12><div class=thumbnail><img src='"+ guru.optString("PictureURL") + "' alt='Imagehere'/><center>" + guru.optString("FirstName") + " " + guru.optString("LastName") + "</center><br><center><b>Score:</b><i>"+guru.optDouble("Score") +"</i></center></div></li>";
//              returnString += "<img src='"+ guru.optString("PictureURL")+"'></img>";
//              returnString += guru.optString("FirstName") + " " + guru.optString("LastName");
//              returnString += "<br>";              
          }
          returnString += "</ul>";
          JSONSearchGurus += returnString;
     }
     catch (Exception exception)
     {        
         Logger.getLogger(GurukadakshOperations.class.getName()).log(Level.SEVERE, "Exception is {0}", exception);
         return "";
     }
     finally
     {
//         return JSONSearchGurus;
         return returnString;
     }
 }
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public static String makeGetConnection(String parameterString, String url)
    {
        return makeConnection(parameterString, url, 0);
    }

    public static String makePostConnection(String parameterString, String url)
    {
        return makeConnection(parameterString, url, 1);
    }

    public static String makeConnection( String parameterString, String url, int connectionType)
    {
        String returnValue = "";
        try
        {
            HttpURLConnection connection = null;

            switch (connectionType)
            {
                case 0: ///< Get request
                {
                    
                    url = appendParametersToUrl(parameterString, url);
                    URL connectTo = new URL(url);
                    connection = (HttpURLConnection) connectTo.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(true);

                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestMethod("GET");
                }
                    break;
                case 1: ///< Post request
                {
                     URL connectTo = new URL(url);
                     connection = (HttpURLConnection) connectTo.openConnection();
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    connection.setInstanceFollowRedirects(true);

                    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    connection.setRequestProperty("charset", "utf-8");
                    connection.setRequestProperty("Content-Length", "" + Integer.toString(parameterString.getBytes().length));
                    connection.setRequestMethod("POST");
                    DataOutputStream wr = new DataOutputStream(connection.getOutputStream ());
                    wr.writeBytes(parameterString);
                    wr.flush();
                    wr.close();
                }
                    break;
                case 2: ///< Put request
                    break;
                default:
                    throw new Exception();

            }
            DataInputStream inStream = new DataInputStream(connection.getInputStream());
            String buffer;
            while((buffer = inStream.readLine()) != null)
            {
                returnValue += buffer + "\n";
            }
            connection.disconnect();
        }
        catch(Exception e)
        {
            returnValue = "Exception is " + e;
        }
        finally
        {
            return returnValue;
        }
    }
    /**
     * This function is used to append get parametes to the the url.
     * @param parameterString
     * @param urlString
     * @return
     */
    public static String appendParametersToUrl(String parameterString, String urlString)
    {
        if (urlString.contains("?"))
        {
            return urlString.substring(0, urlString.indexOf("?")) + parameterString+ ((urlString.endsWith("?"))? "&"+urlString.substring(urlString.indexOf("?")+1):"");
        }
        else
        {
            return urlString + "?" + parameterString;
        }
    }
}