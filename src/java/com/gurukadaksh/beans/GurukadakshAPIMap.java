/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.gurukadaksh.beans;

import javax.ejb.Singleton;

/**
 *
 * @author Deepak Subramanian
 */
@Singleton
public class GurukadakshAPIMap
{
    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    public class General
    {
        public static final String protocol = "http";
        public static final String serverAddress = "pre-alpha.gurubyo.com";
        public static final String portNumber = "80";
        public static final String endpoint = "/api";
        public static final String endpoint_URL = protocol + "://" + serverAddress + ":" + portNumber + endpoint;
        public class URLParams
        {
            public static final String gkToken = "gkToken";
        }
    }
    
    public class LoginApiUrl
    {
        public static final String loginPage = "/loginUrl";
        public static final String loginPage_URL = General.endpoint_URL + loginPage;
        public class POSTParams
        {
            public static final String callback = "redirectUrl";
        }
    }
    
    public class UserProfileApiUrl
    {
        public static final String userProfilePage = "/userProfile";
        public static final String userProfilePage_URL = General.endpoint_URL + userProfilePage;
    }
    
    public class SkillsSearchApiUrl
    {
        public static final String skillsSearchPage = "/searchSkills";
        public static final String skillsSearchPage_URL = General.endpoint_URL + skillsSearchPage;
        public class POSTParams
        {
            public static final String skillsToSearch = "q";          
        }
    }
    
    public class CreateProjectApiUrl
    {
        public static final String createProjectPage = "/createProject";
        public static final String createProjectPage_URL = General.endpoint_URL + createProjectPage;
        public class POSTParams
        {
            public static final String projectName = "title";
            public static final String projectDescription = "description";
            public static final String projectKeywords = "keywords";
        }
    }
    
    public class GuruSearchApiUrl
    {
        public static final String guruSearchPage = "/searchGurus";
        public static final String guruSearchPage_URL = General.endpoint_URL + guruSearchPage;
        public class POSTParams
        {
            public static final String projectId = "projectId";
        }
    }

    public class UserTagApiUrl
    {
        public static final String userTagPage = "/userTags";
        public static final String userTagPage_URL = General.endpoint_URL + userTagPage;
    }
    
    public class JSONParameters
    {
        public static final String AUTH_URL = "linkedinURL";
        public static final String USER_FIRSTNAME = "First Name";
        public static final String USER_LASTNAME = "Last Name";
        public static final String USER_HEADLINE = "Headline";
        public static final String USER_PROFILEPIC = "Profile Picture Url";
    }
    
}
