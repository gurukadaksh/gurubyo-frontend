<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="gurukadakshBean" class="com.gurukadaksh.beans.GurukadakshOperations" scope="session"/>
<%  if (!gurukadakshBean.isLoggedIn()) {
        try {
            response.sendRedirect(request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/Login.jsp");
        } catch (IOException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Exception on UI - is {0}", ex);
        }
    } else {
        String projectId = request.getParameter("projectId");
        if (projectId != null && !projectId.trim().isEmpty()) {
%>
<!DOCTYPE html>
<html>
    <head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>GuruBYo's Gurus!</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="This is a page for creating a project">
        <meta name="author" content="Deepak Subramanian">

        <!-- Le styles -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
        <!-- All stylesheets here -->    
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/black-tie/jquery-ui.css" type="text/css" />

        <!-- External scripts here -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>
        <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js'></script>
        <script src ="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.1.0.js"></script>
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>

        <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a href="/" class="brand"><img alt="Logobro_nav" id="topbar_logo" src="assets/gurubyoLogoFullInverted.png" width="200" height="20"/></a>
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>	
                    <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <li class="dropdown" id="menu1">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">
                                <img src="<%=gurukadakshBean.getUserProfilePicture()%>"  />
                                Hi, <%=gurukadakshBean.getUserFirstName()%>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="guruSearch.jsp">Guru Search</a></li>
                                <li><a href="User.jsp">Home</a></li>
                                <li class="divider"></li>
                                <li><a href="logout.jsp">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container">
            <br>
            <br>
            <br>
            <br>
            <div class="row">
                <div class="span12"> 
                    <%= gurukadakshBean.searchGurus(Long.parseLong(projectId))%>
                </div>
            </div>
        </div>
    </body>
</html>

<%
} else {
%>
<!DOCTYPE html>
<!-- saved from url=(0066)http://twitter.github.com/bootstrap/examples/starter-template.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>Search for a potential Guru on this page: GuruBYo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="This is a page for creating a project">
        <meta name="author" content="Deepak Subramanian">

        <!-- Le styles -->
        <link href="css/bootstrap.css" rel="stylesheet">
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.9.1/themes/base/jquery-ui.css" />
        <!-- All stylesheets here -->    
        <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/black-tie/jquery-ui.css" type="text/css" />

        <!-- External scripts here -->
        <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js'></script>
        <script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/jquery-ui.min.js'></script>
        <script src ="http://ajax.aspnetcdn.com/ajax/knockout/knockout-2.1.0.js"></script>
        <style>
            body {
                padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
            }
        </style>

        <script>
            var map = new Object();
            var keywordsArray = new Array();
            $(function() {
                $( "#tags" ).autocomplete({
                    source: function( request, response ) {
                            
                        url = "http://localhost:8080/api/searchSkills?i=2&q=" + request.term;
                        //url = "http://www.giveupalready.com/demos/jquery/remote.php?term=" + request.term;
                            
                        $.getJSON(url + '&callback=?', function(data) {
                            var newArray = new Array();
                                
                            for (i=0; i<data["results"].length; i++){
                                console.log(data["results"][i]["name"]);
                                newArray.push(data["results"][i]["name"]);
                                map[data["results"][i]["name"]] = data["results"][i];
                            }
                            response(newArray);
                        });
                    }
                });
            });

            // Overall viewmodel for this screen, along with initial state
            function KeywordsViewModel() {
                var self = this;


                // Editable data
                self.keywordsKoList = ko.observableArray([

                ]);
                self.addKeyword = function() {
                    try
                    {
                        var keyword = document.getElementById("tags").value;    
                        var pushVal = map[keyword];
                        if (typeof(pushVal) !== 'undefined')
                        {
                            self.keywordsKoList.push(pushVal);                      
                        
                            document.getElementById("tags").value = "";
                        }
                        else
                        {
                            alert("The keyword was not found .. Kindly try to select one from our list.." + err);
                        }
                    }
                    catch (err)
                    {
            <%-- To catch key not found exceptions --%>
                            alert("The keyword was not found .. Kindly try to select one from our list.." + err);
                        }
                    }
                    self.removeKeyword = function(kayword) { self.keywordsKoList.remove(kayword) }
                    // Computed data
                    self.boundArrayString = ko.computed(function() {
                        return JSON.stringify(ko.toJS(self.keywordsKoList));
                    });
                    self.getArrayString = function() {
                        return JSON.stringify(ko.toJS(self.keywordsKoList));
                    }
                    self.createProject = function()
                    {
                        $('<form action="CreateProject.jsp" method="POST"/>')
                        .append('<input type="hidden" name="title" value=\'' + document.getElementById("title").value + '\'>')
                        .append('<input type="hidden" name="description" value=\'' + document.getElementById("desc").value + '\'>')
                        .append('<input type="hidden" name="keywords" value=\'' + JSON.stringify(ko.toJS(self.keywordsKoList)) + '\'>') /*JSON.stringify(keywordsArray) + '\'>')*/
                        .appendTo($(document.body))
                        .submit();
                    }
                }

            
        </script>
        <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">

        <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Le fav and touch icons -->
        <link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container">
                    <a href="/" class="brand"><img alt="Logobro_nav" id="topbar_logo" src="assets/gurubyoLogoFullInverted.png" width="200" height="20"/></a>
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>

                    <ul class="nav pull-right">
                        <li class="divider-vertical"></li>
                        <li class="dropdown" id="menu1">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">
                                <img src="<%=gurukadakshBean.getUserProfilePicture()%>"  />
                                Hi, <%=gurukadakshBean.getUserFirstName()%>
                                <b class="caret"></b>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="guruSearch.jsp">Guru Search</a></li>
                                <li><a href="User.jsp">Home</a></li>
                                <li class="divider"></li>
                                <li><a href="logout.jsp">Sign out</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container">
            <br>
            <br>
            <br>
            <br>

            <div id="content">
                <c:if test="${not empty message}">
                    ${uploadStatus}
                </c:if>
                <div class="row">
                    <div class="tabbable span12">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tabs1-pane1" data-toggle="tab">Describe</a></li>
                            <li><a href="#tabs1-pane2" data-toggle="tab">Upload</a></li>
                            <li><a href="#tabs1-pane3" data-toggle="tab">URLize</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs1-pane1">
                                <div class="span6 well"> 
                                    <h4>Describe your problem</h4>
                                    <legend>Describe your goal</legend>				  				  
                                    <input class="input-xxlarge span6" type="text" placeholder="Your goal" name="title" id="title"><br/>
                                    <span class="label label-success">Tip</span><label>Use a pencil and paper to jot down</label>
                                    <legend>What is your task at hand?</legend>				  
                                    <!--<input class="input-xxlarge" type="text" placeholder="Tip:Use a pen and paper" name="description" id="desc">-->
                                    <textarea style="resize:none" class="field span6" rows="6" placeholder="Describe the task you need to accomplish" name="description" id="desc"></textarea><br/>
                                    <span class="label label-success">Tip</span>  
                                    <span class="help-block">Express better to find a better guru
                                        Our make sense of whatever information you provide. So be as descriptive as possible. Use a paper and pencil to jot down your problem. Draw some figures. Connect some dots and you should be good to go.</span>				  				
                                    <legend>Keywords</legend>					
                                    <input class="input-xxlarge" type="text" placeholder="Enter Keywords" id="tags" name="keywords">
                                    <span class="label label-success">Tip</span>  
                                    <span class="help-block">Identify key words to help our algorithms to search better. Try to be relevant but don't worry about accuracy. Our algorithms make sense of the keywords. Use a paper and pencil to jot down your keywords</span>
                                    <p></p> 
                                    <table>
                                        <thead><tr>
                                                <th></th><th></th>

                                            </tr></thead>
                                        <!-- Todo: Generate table body -->
                                        <tbody data-bind="foreach: keywordsKoList">
                                            <tr>
                                                <td data-bind="text: name" />
                                                <!--        <td><select data-bind="options: $root.availableMeals, value: meal, optionsText: 'mealName'"></select></td>-->
                                                <!--        <td data-bind="text: formattedPrice"></td>-->
                                                <td><a href="#" data-bind="click: $root.removeKeyword">[×]</a></td>
                                            </tr>    
                                        </tbody>
                                    </table>
                                    <h3>
                                        DEBUG DATA : <span data-bind="text: boundArrayString()"> </span>
                                    </h3>

                                    <button data-bind="click: addKeyword">Add Keyword</button>  
                                    <!--                        <p><input type="submit" class="btn btn-primary btn-large" value="Step 2 »"></input></p>					-->
                                    <!--                    </form>-->
                                    <p><a href="#"<%--"javascript:createProject()"--%> data-bind="click: createProject" role="button" class="btn btn-primary btn-large">Step 2 »</a></p>
                                </div>
                                <div class = "span4">			
                                    <img src="assets/goal.jpg">
                                    <h4><em>"Goal: I want to be a professional guitarist"</em></h4>

                                    <img src="assets/objective.jpg">
                                    <h4><em>"Task at hand: I am recording a portfolio album for auditions"</em></h4>
                                </div>

                            </div>
                            <div class="tab-pane" id="tabs1-pane2">
                                <div class="span6 well"> 
                                    <h4>Upload a document</h4>
                                    <p>
                                        Select a file to upload: <form action="fileuploadexample" method="post" enctype="multipart/form-data">
                                            <p><input type="file" name="file" size="50" class="btn"/> <input type="submit" value="Upload" class="btn btn-primary" /></p>
                                        </form>
                                    </p>
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs1-pane3">
                                <div class="span8 well"> 
                                    <h4>Pass a URL to GuruByo</h4>
                                    <p>                                        
                                        <input class="input-xxlarge span6" type="text" placeholder="http://www.ntu.edu.sg/AboutNTU/Achievements/Performance/Pages/Scalingnewheightsinacademia.aspx" name="url" id="url">
                                        <a href="#" role="button" class="btn btn-primary btn-primary">Parse URL »</a>
                                        <br/>
                                        <span class="label label-success">Tip</span><label>Paste a URL that you'd like GuruByo to parse</label>
                                    </p>
                                </div>
                            </div>
                        </div><!-- /.tab-content -->
                    </div><!-- /.tabbable -->
                </div>
            </div> <!-- /container -->

            <!-- Le javascript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <script src="./simple_files/bootstrap-modal.js"></script>
            <script src="./simple_files/bootstrap-dropdown.js"></script>
            <script>ko.applyBindings(new KeywordsViewModel());</script>
            <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
            <script>window.jQuery || document.write('<script src="js/jquery-1.7.1.min.js"><\/script>')</script>
            <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
            <script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
            <!-- Bootstrap JS: compiled and minified -->
            <script src="js/bootstrap.min.js"></script>
<!--
             Example plugin: Prettify 
            <script src="js/prettify/prettify.js"></script>-->

<!--             Initialize Scripts 
            <script>
                // Activate Google Prettify in this page
                addEventListener('load', prettyPrint, false);
		
                $(document).ready(function(){
                    // Add prettyprint class to pre elements
                    $('pre').addClass('prettyprint');
                }); // end document.ready
            </script>-->
    </body>
</html>
<%}
    }%>