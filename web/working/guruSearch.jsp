<!DOCTYPE html>
<!-- saved from url=(0066)http://twitter.github.com/bootstrap/examples/starter-template.html -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>Bootstrap, from Twitter</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
    <style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
		  <a href="/" class="brand"><img alt="Logobro_nav" id="topbar_logo" src="assets/gurukadakshThumbnailLogoV560px.png" /></a>
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>		  	  
          <a class="brand" href="http://twitter.github.com/bootstrap/examples/starter-template.html#"></a>
        </div>
      </div>
    </div>


    <div class="container">
    	<br>
        <br>
		<div class="row">
			<div class="span6 well">
				<h1>Step 1 of 2</h1>				
				<form>				  
				  <legend>Describe your goal</legend>				  				  
				  <input class="input-xxlarge" type="text" placeholder="Your Goal">
				  <label>Tip: use a pencil and paper to jot down</label>
				</form>
				<form>
				  <legend>What is your task at hand?</legend>				  
				  <input class="input-xxlarge" type="text" placeholder="Tip:Use a pen and paper">
				  <span class="label label-success">Tip</span>  
				  <span class="help-block">Express better to find a better guru
Our make sense of whatever information you provide. So be as descriptive as possible. Use a paper and pencil to jot down your problem. Draw some figures. Connect some dots and you should be good to go.</span>				  				

				</form>
				<form>
					<legend>Keywords</legend>					
					<input class="input-xxlarge" type="text" placeholder="Enter Keywords">
					<span class="label label-success">Tip</span>  
					<span class="help-block">Identify key words to help our algorithms to search better. Try to be relevant but don't worry about accuracy. Our algorithms make sense of the keywords. Use a paper and pencil to jot down your keywords</span>
					<p></p>
					<p><a class="btn btn-primary btn-large">Step 2 »</a></p>					
				</form>
			</div>
			
			<div class = "span4">			
			<img src="assets/goal.jpg">
			<h4><em>"Goal: I want to be a professional guitarist"</em></h4>
			
			<img src="assets/objective.jpg">
			<h4><em>"Task at hand: I am recording a portfolio album for auditions"</em></h4>
			</div>
			
		</div>
	</div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./guruSearch_files/jquery.js"></script>
    <script src="./guruSearch_files/bootstrap-transition.js"></script>
    <script src="./guruSearch_files/bootstrap-alert.js"></script>
    <script src="./guruSearch_files/bootstrap-modal.js"></script>
    <script src="./guruSearch_files/bootstrap-dropdown.js"></script>
    <script src="./guruSearch_files/bootstrap-scrollspy.js"></script>
    <script src="./guruSearch_files/bootstrap-tab.js"></script>
    <script src="./guruSearch_files/bootstrap-tooltip.js"></script>
    <script src="./guruSearch_files/bootstrap-popover.js"></script>
    <script src="./guruSearch_files/bootstrap-button.js"></script>
    <script src="./guruSearch_files/bootstrap-collapse.js"></script>
    <script src="./guruSearch_files/bootstrap-carousel.js"></script>
    <script src="./guruSearch_files/bootstrap-typeahead.js"></script>

  

</body></html>