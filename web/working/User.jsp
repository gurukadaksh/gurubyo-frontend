<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!-- saved from url=(0054)http://twitter.github.com/bootstrap/examples/hero.html -->
<jsp:useBean id="gurukadakshBean" class="com.gurukadaksh.beans.GurukadakshOperations" scope="session"/>
<% gurukadakshBean.setGKToken(request,response); if(!gurukadakshBean.isLoggedIn())
        {
            try {
                response.sendRedirect(request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/Login.jsp");
            } catch (IOException ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
            }
        } else{%>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>gurukadaksh</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
	<link href="css/metro.css" rel="stylesheet">
    <link href="css/start.less?v=4" rel="stylesheet/less" type="text/css" >
    <link href="css/representative.css" rel="stylesheet" media="all">
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
    </style>
    <link href="http://twitter.github.com/bootstrap/assets/css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>

  	<% gurukadakshBean.getUserProfile(); %>
   <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
		  <a href="/" class="brand"><img alt="Logobro_nav" id="topbar_logo" src="assets/gurukadakshThumbnailLogoV560px.png" /></a>
          
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>		  	  
         
      	   <ul class="nav pull-right">
	            <li class="divider-vertical"></li>
	            <li class="dropdown" id="menu1">
	                <a class="dropdown-toggle" data-toggle="dropdown" href="#menu1">
	                    <img src="<%=gurukadakshBean.getUserProfilePicture()%>" height="60" />
	                    Hi, <%=gurukadakshBean.getUserFirstName()%>
	                    <b class="caret"></b>
	                </a>
	                <ul class="dropdown-menu">
	                    <li><a href="#">Settings</a></li>
	                    <li><a href="#">Help</a></li>
	                    <li class="divider"></li>
	                    <li><a href="#">Sign out</a></li>
	                </ul>
	            </li>
	        </ul>

	      </div>
	  </div>
	</div>



    <div class="container">
		<br>
        <br>
        <br>      
		<div class="row">
			<div class="span7">
				<div class="span6 well">
				<h2>Start your mentor search here</h2>
				<p>Think about the problem you want to solve, the project you want to finish, the language you want to learn. Our search algorithms try to make sense of your problem at hand and try to connect you to your mentor.</p>
				<p><a class="btn btn-primary btn-large">Start mentor search »</a></p>
				</div>
		
		
		
			<div class="span3">
				<h3>Classroom</h3>
				<div class="bs-docs-example">
				  <div id="myCarousel" class="carousel slide">
					<div class="carousel-inner">
					  <div class="item">
						<img src="./Javascript · Twitter Bootstrap_files/bootstrap-mdo-sfmoma-01.jpg" alt="">					
						<div class="carousel-caption">
						  <h4>Project 1 title</h4>
						  <p>Project description</p>
						</div>
					  </div>
					  <div class="item">
						<img src="./Javascript · Twitter Bootstrap_files/bootstrap-mdo-sfmoma-02.jpg" alt="">
						<div class="carousel-caption">
						  <h4>Project 2 title</h4>
						  <p>Project Description</p>
						</div>
					  </div>
					  <div class="item active">
						<img src="./Javascript · Twitter Bootstrap_files/bootstrap-mdo-sfmoma-03.jpg" alt="">
						<div class="carousel-caption">
						  <h4>Project 3 title</h4>
						  <p>Project description</p>
						</div>
					  </div>
					</div>
					<a class="left carousel-control" href="http://twitter.github.com/bootstrap/javascript.html#myCarousel" data-slide="prev">‹</a>
					<a class="right carousel-control" href="http://twitter.github.com/bootstrap/javascript.html#myCarousel" data-slide="next">›</a>
				</div>
				</div>
			</div>
			
			<div class="span3">
				<h3>Messages</h3>
				<div class="bs-docs-example">
				  <div id="myCarousel" class="carousel slide">
					<div class="carousel-inner">
					  <div class="item">
						<img src="./Javascript · Twitter Bootstrap_files/bootstrap-mdo-sfmoma-01.jpg" alt="">					
						<div class="carousel-caption">
						  <h4>Project 1 title</h4>
						  <p>Project description</p>
						</div>
					  </div>
					  <div class="item">
						<img src="./Javascript · Twitter Bootstrap_files/bootstrap-mdo-sfmoma-02.jpg" alt="">
						<div class="carousel-caption">
						  <h4>Project 2 title</h4>
						  <p>Project Description</p>
						</div>
					  </div>
					  <div class="item active">
						<img src="./Javascript · Twitter Bootstrap_files/bootstrap-mdo-sfmoma-03.jpg" alt="">
						<div class="carousel-caption">
						  <h4>Project 3 title</h4>
						  <p>Project description</p>
						</div>
					  </div>
					</div>
					<a class="left carousel-control" href="http://twitter.github.com/bootstrap/javascript.html#myCarousel" data-slide="prev">‹</a>
					<a class="right carousel-control" href="http://twitter.github.com/bootstrap/javascript.html#myCarousel" data-slide="next">›</a>
				  </div>
				</div>
			</div>
		
			</div>
			
			<div class="span4 well">
				<h2>Project feed</h2>
				<div class="accordion" id="accordion2">
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseOne">
							Create the next gen product
							</a>
						</div>
						<div id="collapseOne" class="accordion-body collapse in">
							<div class="accordion-inner">
								Develop a abc of 2345
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
					
					<div class="accordion-group">
						<div class="accordion-heading">
							<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo">
							Create a kick ass algo to ???
							</a>
						</div>
						<div id="collapseTwo" class="accordion-body collapse">
							<div class="accordion-inner">
								This algo is amazing
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		

      <hr>

      <footer>
        <p>© gurukadaksh.com 2012</p>
      </footer>

    </div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./simple_files/jquery.js"></script>
    <script src="./simple_files/bootstrap-transition.js"></script>
    <script src="./simple_files/bootstrap-alert.js"></script>
    <script src="./simple_files/bootstrap-modal.js"></script>
    <script src="./simple_files/bootstrap-dropdown.js"></script>
    <script src="./simple_files/bootstrap-scrollspy.js"></script>
    <script src="./simple_files/bootstrap-tab.js"></script>
    <script src="./simple_files/bootstrap-tooltip.js"></script>
    <script src="./simple_files/bootstrap-popover.js"></script>
    <script src="./simple_files/bootstrap-button.js"></script>
    <script src="./simple_files/bootstrap-collapse.js"></script>
    <script src="./simple_files/bootstrap-carousel.js"></script>
    <script src="./simple_files/bootstrap-typeahead.js"></script>

  

</body></html>
<%}%>