<!DOCTYPE html>
<!-- saved from url=(0066)http://twitter.github.com/bootstrap/examples/starter-template.html -->

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
   "http://www.w3.org/TR/html4/loose.dtd">
<jsp:useBean id="gurukadakshBean" class="com.gurukadaksh.beans.GurukadakshOperations" scope="session"/>
<% if(gurukadakshBean.isLoggedIn()) response.sendRedirect(request.getScheme()+"://"+request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/User.jsp");%>

<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>gurukadaksh</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="css/bootstrap.css" rel="stylesheet">
    <style>
	h1 {text-align:center;}
	p.text {text-align:center;}
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
    </style>
    <link href="css/bootstrap-responsive.css" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="http://twitter.github.com/bootstrap/assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="http://twitter.github.com/bootstrap/assets/ico/apple-touch-icon-57-precomposed.png">
  </head>

  <body>


    <div class="container">            
		
            <div class="row-fluid">				
				<br>
				<br>
				<br>
				
                <div class = "span4 offset4">
                    <img src="assets/gurukadakshLogo400px2Ver2.png" class="img-rounded">
                    <br>
					<br>
					<br>
					<br>
					<br>
                    <br>
                </div>
			</div>
			
            <div class = "span8 offset2">				
				<h1>everyone's a guru!</h1>
				<p class="text">Think about the problem you want to solve, the project you want to finish, the language you want to learn. Our search algorithms try to make sense of your problem at hand and try to connect you to your mentors around you.</p>
				
            </div>
            
			<div class="row-fluid well">	
				<div class = "span2 offset5">
				<img src="assets/linkedinLogo100px.jpg" class="img-rounded">
				
				</div>
				<div class = "span4">				
				<p><a href ="<%=gurukadakshBean.getAuthenticationUrl(request)%>" class="btn btn-large btn-block btn-primary">Login»</a></p> 
				</div>

			</div>
		<footer>
			<br>

			<br>
			<br>
			<br>
			<br>
			<hr>	
			<p>© gurukadaksh.com 2012</p>
		</footer>
        
	</div> <!-- /container -->

    <!-- Le javascript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="./guruSearch_files/jquery.js"></script>
    <script src="./guruSearch_files/bootstrap-transition.js"></script>
    <script src="./guruSearch_files/bootstrap-alert.js"></script>
    <script src="./guruSearch_files/bootstrap-modal.js"></script>
    <script src="./guruSearch_files/bootstrap-dropdown.js"></script>
    <script src="./guruSearch_files/bootstrap-scrollspy.js"></script>
    <script src="./guruSearch_files/bootstrap-tab.js"></script>
    <script src="./guruSearch_files/bootstrap-tooltip.js"></script>
    <script src="./guruSearch_files/bootstrap-popover.js"></script>
    <script src="./guruSearch_files/bootstrap-button.js"></script>
    <script src="./guruSearch_files/bootstrap-collapse.js"></script>
    <script src="./guruSearch_files/bootstrap-carousel.js"></script>
    <script src="./guruSearch_files/bootstrap-typeahead.js"></script>

  

</body></html>