<%-- 
    Document   : logout
    Created on : Nov 7, 2012, 6:24:37 PM
    Author     : deepaksubramanian
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<% session.invalidate(); %>
<% response.sendRedirect("Login.jsp"); %>