<%-- 
    Document   : CreateProject.jsp
    Created on : Nov 27, 2012, 6:17:51 PM
    Author     : deepaksubramanian
--%>

<%@page import="java.util.logging.Logger"%>
<%@page import="java.util.logging.Level"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="gurukadakshBean" class="com.gurukadaksh.beans.GurukadakshOperations" scope="session"/>
<%  if(!gurukadakshBean.isLoggedIn())
        {
            try 
            {
                response.sendRedirect(request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/Login.jsp");
            } 
            catch (IOException ex) 
            {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "Exception on UI - is {0}", ex);
            }
        }
        else
        { 
            response.setHeader("Refresh","1; url="+request.getScheme() + "://" + request.getServerName() + ":" + request.getLocalPort() + request.getContextPath() + "/guruSearch.jsp?projectId="+gurukadakshBean.createProject(request));
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Create Project.</title>
    </head>
    <body>
        <h1>
            <%=  "A project has been created .. "+"<br>"+"Redirecting ... " %>
        </h1>
    </body>
</html>
<% }%>